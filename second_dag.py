import datetime

from airflow import DAG
from airflow.operators.empty import EmptyOperator

with DAG(
     dag_id="second_dag",
     start_date=datetime.datetime(2023, 11, 11),
     schedule="@weekly",
 ):
     EmptyOperator(task_id="task")
